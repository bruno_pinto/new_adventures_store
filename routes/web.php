<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/cart/{product}', 'CartController@store');
Route::put('/cart/{product}', 'CartController@update');
Route::delete('/cart/{product}', 'CartController@destroy');
Route::get('/cart', 'CartController@index')->name('cart');
Route::post('/checkout', 'CartController@checkout')->name('checkout');

//set currency
Route::get('currency/{currency}', function ($currency) {
    Session::put('currency', $currency);
    return redirect()->back();
});

//set language
Route::get('language/{locale}', function ($locale) {
    Session::put('locale', $locale);
    return redirect()->back();
});

