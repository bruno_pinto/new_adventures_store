<?php

return [

    'add_to_cart' => 'Adicionar ao carrinho',
    'empty_cart' => 'Parece que o seu carrinho está vazio',
    'start_shopping_now' => 'Comece a comprar agora!',
    'quantity' => 'Quantidade',
    'name' => 'Nome',
    'price' => 'Preço',
    'subtotal' => 'Subtotal',
    'remove' => 'remover',
    'total' => 'Total',
    'buy' => 'Comprar Agora',
    'thankyou' => 'Obrigado pela sua compra',
    'email_sent' => 'Enviamos-lhe um email com os detalhes da encomenda',
    'continue_shopping' => 'Continuar a comprar',
    'added_to_cart' => 'foi adicionado ao seu carrinho',
];
