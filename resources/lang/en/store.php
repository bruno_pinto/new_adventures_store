<?php

return [

    'add_to_cart' => 'Add to cart',
    'empty_cart' => 'Looks like your cart is empty.',
    'start_shopping_now' => 'Start shopping now!',
    'quantity' => 'Quantity',
    'name' => 'Name',
    'price' => 'Price',
    'subtotal' => 'Subtotal',
    'remove' => 'remove',
    'total' => 'Total',
    'buy' => 'Buy Now',
    'thankyou' => 'Thank you for your purchase',
    'email_sent' => 'An email has been sent with your order details',
    'continue_shopping' => 'Continue shopping',
    'added_to_cart' => 'was added to your cart',
];
