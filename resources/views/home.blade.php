@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach($products as $product)
        <div class="col-md-4">
            <div class="card text-center pt-4" style="width: 18rem;">
                <img class="card-img-top" src="{{ asset('images/' . $product->photo) }}" alt="{{ $product->name }}">
                <div class="card-body">
                <h5 class="card-title">{{ $product->name }}</h5>
                <p class="card-text">{{ $product->price }}</p>

                @guest
                <a href="{{ route('login') }}" class="btn btn-primary">{{ __('store.add_to_cart') }}</a>
                @else
                    <a href="#" class="btn btn-primary" onclick="updateCart({{ $product->id }})">{{ __('store.add_to_cart') }}</a>
                @endguest

                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
