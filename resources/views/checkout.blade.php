@extends('layouts.app')

@section('content')

<div class="checkout text-center">
    <img class="mb-3" src="{{ asset('images/thankyou.jpeg') }}">
    <h3>{{ __('store.thankyou') }}</h3>
    <h5>{{ __('store.email_sent') }}</h5>
    <a href="{{ route('home') }}" class="btn btn-primary">{{ __('store.continue_shopping') }}</a>
</div>

@endsection
