<div class="alert alert-light cart-success" role="alert">
    {{ $product->name }} {{__('store.added_to_cart')}}
</div>
