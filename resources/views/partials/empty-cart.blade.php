<div class="empty-cart text-center">
    <img class="mb-3" src="{{ asset('images/empty-cart.jpeg') }}">
<h3>{{ __('store.empty_cart') }}</h3>
<a href="{{ route('home') }}" class="btn btn-primary">{{ __('store.start_shopping_now') }}</a>
</div>
