@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if($items->count())
        <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">{{ __('store.quantity') }}</th>
                <th scope="col">{{ __('store.name') }}</th>
                <th scope="col">{{ __('store.price') }}</th>
                <th scope="col">{{ __('store.subtotal') }}</th>
                <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($items as $item)
                    <tr class="item" id="item-{{$item->id}}">
                    <th scope="row">
                        <input type="number" value="{{ $item->quantity }}" onchange="updateQuantity({{ $item->id }}, this.value)">
                    </th>
                    <td>{{ $item->product->name }}</td>
                    <td>{{ $item->product->price }}</td>
                    <td class="subtotal">{{ $item->subtotal }}</td>
                    <td>
                        <a href="#" class="btn btn-sm btn-primary" onclick="deleteItem({{ $item->id }})">{{ __('store.remove') }}</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">{{ __('store.total') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row" class="total">{{ $total }}</th>
                </tr>
            </tbody>
        </table>

        <a href="#" class="btn btn-primary" onclick="$('#checkout').submit()">{{ __('store.buy') }}</a>
        <form method="POST" action="{{ route('checkout') }}" id="checkout">
            @csrf
        </form>
        @else
            @include('partials.empty-cart')
        @endif
    </div>
</div>
@endsection
