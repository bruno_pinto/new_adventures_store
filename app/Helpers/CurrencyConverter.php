<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Cache;

class CurrencyConverter
{
    public function __construct($ammount) {
        $this->currency = session('currency') ? strtoupper(session('currency')) : 'EUR';
        $this->ammount = $ammount;
    }

    public function convert() {

        if($this->currency == 'EUR') {
            return $this->ammount . ' €';
        }

        if(session('currency') == 'brl' && Cache::has('rate_brl')) {
            return $this->toBRL();
        }

        if(session('currency') == 'usd' && Cache::has('rate_usd')) {
            return $this->toUSD();
        }

        $XML = simplexml_load_file("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");

        foreach($XML->Cube->Cube->Cube as $rate){
            if($rate['currency'] == $this->currency) {
                $converted = number_format($this->ammount * floatval($rate['rate']), 2);
                if(session('currency') == 'usd') {
                    Cache::add('rate_usd', floatval($rate['rate']), 1440);
                    return '$ ' . $converted;
                }
                if(session('currency') == 'brl') {
                    Cache::add('rate_brl', floatval($rate['rate']), 1440);
                    return $converted . ' R$';
                }
            }
        }
    }

    public function toBRL() {
        return number_format(Cache::get('rate_brl') * $this->ammount, 2) . ' R$';
    }

    public function toUSD() {
        return '$ ' . number_format(Cache::get('rate_usd') * $this->ammount, 2);
    }
}
