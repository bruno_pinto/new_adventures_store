<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Cart;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*',function($view) {
            if(Auth::check()) {
                $items = Cart::whereUserId(Auth::user()->id)
                ->whereStatus('awaiting_payment')
                ->get();

                $cart_items = 0;

                foreach($items as $item) {
                    $cart_items = $cart_items + $item->quantity;
                }

                $view->with('cart_items', $cart_items);
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
