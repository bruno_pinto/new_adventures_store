<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\CurrencyConverter;

use Illuminate\Support\Facades\Mail;
use App\Mail\OrderShipped;

use App\Product;
use App\Cart;

class CartController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index()
    {
        $items = Cart::whereUserId(Auth::user()->id)
            ->whereStatus('awaiting_payment')
            ->with('product')
            ->get();

        return view('cart', [
            'items' => $items,
            'total' => $this->updateTotal()
        ]);
    }

    public function store(Request $request, Product $product)
    {
        $item = Cart::firstOrCreate([
            'user_id' => Auth::user()->id,
            'product_id' => $product->id,
            'status' => 'awaiting_payment'
        ]);

        $item->increment('quantity');

        return view('partials.cart-success', ['product' => $product]);
    }

    public function update(Request $request, $product)
    {
        //update quantity and subtotal
        $item = Cart::with('product')
            ->find($product);

        $item->update(['quantity' => $request->quantity]);

        //get total
        $data['subtotal'] = $item->subtotal;
        $data['total'] = $this->updateTotal();

        return $data;
    }

    public function destroy(Cart $product)
    {
        $product->delete();
        return $this->updateTotal();
    }

    public function checkout()
    {
        $cart = Cart::whereUserId(Auth::user()->id)
            ->whereStatus('awaiting_payment');

        Mail::to(Auth::user()->email)->send(new OrderShipped($cart->get(), $this->updateTotal()));

        $cart->update(['status' => 'payed']);

        return view('checkout');
    }

    public function updateTotal() {
        //get total
        $items = Cart::whereUserId(Auth::user()->id)
            ->whereStatus('awaiting_payment')
            ->with('product')
            ->get();

        $total = 0;

        foreach($items as $item) {
            $total = $total + ($item->product->price_in_euro * $item->quantity);
        }

        $converter = new CurrencyConverter($total);
        return $converter->convert();
    }
}
