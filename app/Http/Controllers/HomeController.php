<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Product;
use App\Cart;

class HomeController extends Controller
{
    public function index()
    {
        $products = Product::take(5)->get();
        return view('home', ['products' => $products]);
    }
}
