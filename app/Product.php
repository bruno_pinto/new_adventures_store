<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CurrencyConverter;

class Product extends Model
{
    public function getPriceAttribute()
    {
        $converter = new CurrencyConverter($this->price_in_euro);
        return $converter->convert();
    }
}
