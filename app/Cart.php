<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CurrencyConverter;

class Cart extends Model
{
    protected $table = 'cart';
    protected $fillable = ['product_id', 'user_id', 'quantity'];

    public function product() {
        return $this->belongsTo('App\Product');
    }

    public function getSubtotalAttribute()
    {
        $price = $this->product->price_in_euro * $this->quantity;
        $converter = new CurrencyConverter($price);
        return $converter->convert();
    }
}
