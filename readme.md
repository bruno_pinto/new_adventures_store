# New adVentures Store

This project is a recruitment test for new adVentures consisting of a store completed with authentication, shopping cart and email confirmation.

It also has currency and language management.

## Installation

Clone this repository and run

```bash
composer install
php artisan key:generate
php artisan migrate
php artisan db:seed --class=ProductsTableSeeder
```

copy the .env.example to .env and fill your MySQL and Mailtrap credentials

## Usage

```bash
php artisan serve
```
Register a new account to start testing the cart features.

## More info

Currency conversions are managed with [this reference](https://www.ecb.europa.eu/stats/policy_and_exchange_rates/euro_reference_exchange_rates/html/index.en.html).

To increase performance, the rates are stored in cache for a 24 hour period.

## contact
For help or any questions shoot me an [email](mailto:brunoabpinto@gmail.com).

## Try the demo
[demo](https://nastore.oniverso.pt)

## License
[MIT](https://choosealicense.com/licenses/mit/)
