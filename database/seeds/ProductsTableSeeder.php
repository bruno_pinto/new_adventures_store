<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
            'name' => 'iPhone XS',
            'photo' => 'iphonexs.png',
            'price_in_euro' => 1099,
            ],
            [
            'name' => 'Samsung Galaxy S9',
            'photo' => 'galaxys9.png',
            'price_in_euro' => 650,
            ],
            [
            'name' => 'Xiaomi Mi Mix 3',
            'photo' => 'mimix3.png',
            'price_in_euro' => 450,
            ],
            [
            'name' => 'Huawei P20',
            'photo' => 'huaweip20.png',
            'price_in_euro' => 439,
            ],
            [
            'name' => 'OnePlus 6',
            'photo' => 'oneplus6.png',
            'price_in_euro' => 495,
            ]
        ]);
    }
}
