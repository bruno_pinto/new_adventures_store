const token = $('meta[name=csrf-token]').attr('content');

function updateCart(productID) {
    currentItemCount = parseInt($('.cart-counter').text());
    $('.cart-counter').text(currentItemCount + 1);

    $.post('/cart/' + productID,
    {
        _token: token
    })
    .done(function(response) {
        $('body').append(response);
        setTimeout(function() {
            $('.cart-success').addClass('visible');
            setTimeout(function() {
                $('.cart-success').remove();
            }, 2000);
        }, 100);
    })
}

function updateQuantity(itemID, updated_quantity) {

    $.post('/cart/' + itemID,
    {
        _method: 'PUT',
        _token: token,
        quantity: updated_quantity
    })
    .done(function(response) {
        $('#item-' + itemID).find('.subtotal').text(response.subtotal);
        $('.total').text(response.total);
    })
}

function deleteItem(itemID) {
    $.post('/cart/' + itemID,
    {
        _method: 'DELETE',
        _token: token,
    })
    .done(function(response) {
        $('.total').text(response);
        $('#item-' + itemID).slideUp();
    })
}
